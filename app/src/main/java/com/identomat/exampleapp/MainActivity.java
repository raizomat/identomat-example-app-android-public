package com.identomat.exampleapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.Toast;

import com.identomat.exampleapp.utils.FragmentUtil;
import com.identomat.exampleapp.utils.IdentomatBroadcastReceiver;
import com.identomat.exampleapp.utils.SpinnerAdapter;
import com.raizomat.identomatsdk.Identomat;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by Identomat Inc. on 11/23/2020.
 */
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    public static final String API_KEY = "41_8c932ac012113773e8a8f563e38db764a8b6ed59";
    private Boolean langSelectedByUser = false;
    final String[] ITEMS_VALUE = new String[]{"en", "ka", "es", "ru"};
    final Integer[] IMAGE_ARRAY = {R.drawable.uk_icon, R.drawable.ge_icon,
            R.drawable.es_icon, R.drawable.ru_icon};
    static final Identomat identomatSDK = Identomat.getInstance();
    private IdentomatBroadcastReceiver mReceiver;
    public static Map<String, String> errors;

    static {
        errors = new HashMap<>();
        errors.put("COMPANY_KEY_INVALID", "Invalid Company Key");
        errors.put("ERROR", "Error Occured");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mReceiver = new IdentomatBroadcastReceiver();
        IntentFilter filter = new IntentFilter("com.identomat.Broadcast");
        registerReceiver(mReceiver, filter);

        Spinner spinner = findViewById(R.id.language_spinner);
        SpinnerAdapter adapter = new SpinnerAdapter(this, R.layout.language_value_layout, getResources().getStringArray(R.array.languages), IMAGE_ARRAY);
        spinner.setAdapter(adapter);
        spinner.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                langSelectedByUser = true;
                return false;
            }
        });
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                if (langSelectedByUser) {
                    setApplicationLocale(ITEMS_VALUE[position]);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
            }

        });

        FragmentUtil au = new FragmentUtil(getSupportFragmentManager());
        au.addFragment(R.id.fragment_container, VerificationSelectFragment.newInstance(), null);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    private void setApplicationLocale(String locale) {
        Resources resources = getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            config.setLocale(new Locale(locale.toLowerCase()));
        } else {
            config.locale = new Locale(locale.toLowerCase());
        }
        resources.updateConfiguration(config, dm);
        recreate();
    }
}
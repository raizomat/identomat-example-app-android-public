package com.identomat.exampleapp.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import com.identomat.exampleapp.ResultActivity;

/**
 * Created by Identomat Inc. on 11/25/2020.
 */
public class IdentomatBroadcastReceiver extends BroadcastReceiver {

    public IdentomatBroadcastReceiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String sessionKey = intent.getStringExtra("idento session key");
        Intent i = new Intent(context, ResultActivity.class);
        i.putExtra("SESSION_KEY", sessionKey);
        context.startActivity(i);
    }
}
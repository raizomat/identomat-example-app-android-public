package com.identomat.exampleapp.utils;

/**
 * Created by Identomat Inc. on 11/23/2020.
 */
public interface ServerResponseCallback<T>
{
    void serverResponseCallback(T ret);
}
package com.identomat.exampleapp.utils;


import android.os.AsyncTask;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

/**
 * Created by Identomat Inc. on 11/23/2020.
 */
public class NetworkTask extends AsyncTask<Void,Void, String> {
    public static final String IDENTOMAT_API_URL = "https://widget.identomat.com/";
    public static final String REQUEST_SESSION_KEY = "Request session key";
    public static final String REQUEST_RESULT = "Request result";


    private ServerResponseCallback mServerResponseCallback;
    private String mEndpoint;
    private String mUrlParams;
    private String mRequestType;

    public NetworkTask(String endpoint, String urlParams, ServerResponseCallback serverResponseCallback, String requestType){
        this.mEndpoint = endpoint;
        this.mServerResponseCallback = serverResponseCallback;
        this.mUrlParams = urlParams;
        this.mRequestType = requestType;
    }
    @Override
    protected String doInBackground(Void... params){
        return getData(mEndpoint,mUrlParams);
    }
    @Override
    protected void onPostExecute(String response){
        JSONObject js = new JSONObject();
        try {
            js.put("requestType",mRequestType);
            js.put("status", "OK");
            js.put("message", response);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        mServerResponseCallback.serverResponseCallback(js);
    }

    public String getData(String endPoint, String urlParams){
        String response="";
        try {
            URL url = new URL(IDENTOMAT_API_URL+endPoint+"/");
            String urlParameters  = urlParams;
            byte[] postData       = urlParameters.getBytes( StandardCharsets.UTF_8 );
            int    postDataLength = postData.length;
            HttpURLConnection c = (HttpURLConnection) url.openConnection();
            c.setDoInput(true);
            c.setRequestMethod("POST");
            c.setRequestProperty( "Content-Type", "application/x-www-form-urlencoded");
            c.setRequestProperty( "charset", "utf-8");
            c.setRequestProperty( "Content-Length", Integer.toString( postDataLength ));
            c.setDoOutput(true);
            OutputStream output = c.getOutputStream();
            PrintWriter writer = new PrintWriter(new OutputStreamWriter(output),
                    true);
            try( DataOutputStream wr = new DataOutputStream( c.getOutputStream())) {
                wr.write( postData );
            }
            Scanner result = new Scanner(c.getInputStream());
            response = result.nextLine();
            result.close();
        } catch (IOException ex) {
            response = "ERROR";
        }
        return response;
    }
}

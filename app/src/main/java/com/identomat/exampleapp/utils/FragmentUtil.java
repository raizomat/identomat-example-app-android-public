package com.identomat.exampleapp.utils;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

/**
 * Created by Identomat Inc. on 11/23/2020.
 */
public class FragmentUtil {

    FragmentManager mFragmentManager;

    public FragmentUtil(FragmentManager fragmentManager){
        mFragmentManager = fragmentManager;
    }

    public void addFragment(int fragmentContainerId, Fragment fragmentToPut, String tag){
        if (tag==null){
            mFragmentManager.beginTransaction()
                    .replace(fragmentContainerId, fragmentToPut, tag)
                    .commit();
        }else{
            mFragmentManager.beginTransaction()
                    .replace(fragmentContainerId, fragmentToPut, tag).addToBackStack( tag )
                    .commit();
        }

    }

}

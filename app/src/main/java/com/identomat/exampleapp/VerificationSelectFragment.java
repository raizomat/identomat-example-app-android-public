package com.identomat.exampleapp;

import android.os.Bundle;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import com.identomat.exampleapp.databinding.FragmentVerificationSelectBinding;
import com.identomat.exampleapp.utils.NetworkTask;
import com.identomat.exampleapp.utils.ServerResponseCallback;
import com.raizomat.identomatsdk.ClientEnvironment;
import com.raizomat.identomatsdk.IdentomatClientDocType;
import com.raizomat.identomatsdk.SessionSetup;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;

/**
 * Created by Identomat Inc. on 11/23/2020.
 */
public class VerificationSelectFragment extends Fragment implements ServerResponseCallback<JSONObject> {

    FragmentVerificationSelectBinding binding;
    private String mSessionKey = null;

    public static VerificationSelectFragment newInstance() {
        VerificationSelectFragment fragment = new VerificationSelectFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        SessionSetup sesSTP = SessionSetup.getInstance(getResources().getConfiguration().locale.getLanguage());
        sesSTP.clearSetup();
        //sesSTP.setClientEnvironment(ClientEnvironment.DEV);
        sesSTP.setAllowDocumentUpload(true);
        sesSTP.skipLegalAgreement();
        sesSTP.skipLivenessInstructions();
        // color setup
        HashMap<String, String> colors = new HashMap<String, String>();
        colors.put("background_low", "#F2F2F2");
        colors.put("background_high","#FFFFFF");
        colors.put("primary_button","#832B8F");
        colors.put("primary_button_text", "#FFFFFF");
        colors.put("secondary_button", "#FFFFFF");
        colors.put("secondary_button_text", "#832B8F");
        colors.put("text_color_header", "#A4A4A4");
        colors.put("text_color", "#676767");
        colors.put("selector_header", "#832B8F");
        colors.put("document_outer","#832B8F");
        colors.put("iteration_text","#000000");
        colors.put("iteration_outer","#BABABA");
        colors.put("background_low_dark", "#011627");
        colors.put("background_high_dark", "#102C43");
        colors.put("primary_button_dark","#17BEBB");
        colors.put("primary_button_text_dark", "#FFFFFF");
        colors.put("secondary_button_dark", "#011627");
        colors.put("secondary_button_text_dark", "#FFFFFF");
        colors.put("text_color_header_dark", "#FFFFFF");
        colors.put("text_color_dark", "#FFFFFF");
        colors.put("selector_header_dark", "#FFFFFF");
        colors.put("document_outer_dark","#17BEBB");
        colors.put("iteration_text_dark","#000000");
        colors.put("iteration_outer_dark","#BABABA");
        sesSTP.setColors(colors);

        //neutral and smile icons
        sesSTP.setNeutralIcon("custom_neutral");
        sesSTP.setSmileIcon("custom_smile");
        //font
        sesSTP.setHead1Font("fonts/bpg_nino_mtavruli_normal.ttf");
        sesSTP.setHead2Font("fonts/helvetica_neue_light.ttf");
        sesSTP.setBodyFont("fonts/mtavruli.ttf");

        //uploading/verify logo setup
        sesSTP.setLogo(getLogoFileBytes());

        //language setup
        setupLanguage(sesSTP);


        //Document type setup
        sesSTP.addDocumentType(IdentomatClientDocType.ID_CARD);
        sesSTP.addDocumentType(IdentomatClientDocType.PASSPORT);
        sesSTP.addDocumentType(IdentomatClientDocType.DRIVING_LICENSE);
        sesSTP.addDocumentType(IdentomatClientDocType.RESIDENCE_PERMIT);


    }



    private byte[] getLogoFileBytes() {
        InputStream inputStream;
        try {
            inputStream = getActivity().getAssets().open("img/mask.png");
            byte[] buffer = new byte[8192];
            int bytesRead;
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                output.write(buffer, 0, bytesRead);
            }
            return output.toByteArray();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private void setupLanguage(SessionSetup sesSTP) {
        HashMap<String, HashMap<String, String>> strings = new HashMap();

        HashMap<String, String> en = new HashMap();
        //consent fragment
        en.put("identomat_agree_page_title", "consent text");
        en.put("identomat_agree", "agree text");
        en.put("identomat_disagree", "disagree text");
        strings.put("en", en);

        HashMap<String, String> ka = new HashMap();
        ka.put("identomat_agree_page_title", "თანხმობის სათაური");
        ka.put("identomat_agree", "თანხმობის ტექსტი");
        ka.put("identomat_disagree", "უარყოფის ტექსტი");
        strings.put("ka", ka);

        HashMap<String, String> ru = new HashMap();
        ru.put("identomat_agree_page_title", "Текст согласия");
        ru.put("identomat_agree", "Согласительный текст");
        ru.put("identomat_disagree", "Несогласный текст");
        strings.put("ru", ru);

        HashMap<String, String> es = new HashMap();
        es.put("identomat_agree_page_title", "Texto de consentimiento");
        es.put("identomat_agree", "Texto de acuerdo");
        es.put("identomat_disagree", "Texto en desacuerdo");
        strings.put("es", es);

        sesSTP.setStrings(strings);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater,  R.layout.fragment_verification_select,container,false);

        Button livenessBtn = binding.liveness;
        Button photoMatchIdBtn = binding.photoMatchId;

        livenessBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionSetup sesSTP = SessionSetup.getInstance(getResources().getConfiguration().locale.getLanguage());
                sesSTP.clearSetup();
                sesSTP.setLiveness(true);
                sesSTP.setAllowDocumentUpload(true);
                getSessionToken();
            }
        });
        photoMatchIdBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SessionSetup sesSTP = SessionSetup.getInstance(getResources().getConfiguration().locale.getLanguage());
                sesSTP.clearSetup();
                sesSTP.setAllowDocumentUpload(true);
                getSessionToken();
            }
        });

        return binding.getRoot();
    }

    public void getSessionToken(){
        try {
            String encodedFlags = URLEncoder.encode(SessionSetup.getInstance(getResources().getConfiguration().locale.getLanguage()).getFlags().toString(), StandardCharsets.UTF_8.toString());
            new NetworkTask("external-api/begin","company_key="+ MainActivity.API_KEY+"&flags="+encodedFlags, (ServerResponseCallback) this, NetworkTask.REQUEST_SESSION_KEY).execute();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void serverResponseCallback(JSONObject ret) {

        try {
            mSessionKey = ret.getString("message").replaceAll("\"","");

            if (MainActivity.errors.containsKey(mSessionKey)){
                Toast.makeText(getActivity(), MainActivity.errors.get(mSessionKey), Toast.LENGTH_SHORT).show();
            }else{
                MainActivity.identomatSDK.start(getActivity(), SessionSetup.getInstance(getResources().getConfiguration().locale.getLanguage()), mSessionKey);
            }


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
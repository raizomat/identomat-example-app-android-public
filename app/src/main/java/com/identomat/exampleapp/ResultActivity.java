package com.identomat.exampleapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.identomat.exampleapp.databinding.ActivityResultBinding;
import com.identomat.exampleapp.utils.NetworkTask;
import com.identomat.exampleapp.utils.ServerResponseCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

/**
 * Created by Identomat Inc. on 11/25/2020.
 */
public class ResultActivity extends AppCompatActivity implements ServerResponseCallback<JSONObject> {

    private ActivityResultBinding binding;
    private TextView jsonTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_result);
        Intent intent = getIntent();
        String sessionKey = intent.getStringExtra("SESSION_KEY");
        Button tryOnceMoreBtn = binding.tryOnceMoreBtn;
        jsonTextView = binding.jsonTxtView;
        new NetworkTask("external-api/result","session_token="+ sessionKey +"&company_key="+ MainActivity.API_KEY, (ServerResponseCallback) this, NetworkTask.REQUEST_RESULT).execute();
        tryOnceMoreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    public void serverResponseCallback(JSONObject ret) {
        try {
            JSONObject message = new JSONObject(ret.getString("message"));
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);
            params.setMargins(0,0,0,0);
            jsonTextView.setLayoutParams(params);
            printJson(message.toString(),0);
        } catch (JSONException e) {
            try {
                jsonTextView.setGravity(Gravity.LEFT);
                jsonTextView.setText(ret.getString("message").replaceAll("\"",""));
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }
            e.printStackTrace();
        }
    }

    void printJson(String json, int tabs){
        try {
            JSONObject js = new JSONObject(json);
            Iterator keys = js.keys();

            while(keys.hasNext()) {
                String currentDynamicKey = (String)keys.next();
                try {
                    JSONObject currentDynamicValue = js.getJSONObject(currentDynamicKey);
                    jsonTextView.setText(jsonTextView.getText()+"\r\n" + currentDynamicKey+":");
                    printJson(currentDynamicValue.toString(), tabs+1);
                }
                catch (JSONException ex){
                    String tabStr = "";
                    for (int i=0; i<tabs;i++)
                        tabStr +="\t\t";
                    jsonTextView.setText(jsonTextView.getText()+"\r\n"+ tabStr +currentDynamicKey + ": " + js.get(currentDynamicKey));
                }

            }
        } catch (JSONException e) {

            e.printStackTrace();
        }
    }
}
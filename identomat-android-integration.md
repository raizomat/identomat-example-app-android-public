# Integration Document 

Identomat library provides a user identification flow that consists of different steps and involves 
the combination of document type and user image/video.



### Following steps are necessary to start a session with identomat API

1. Add identomat library to the project

2. Add permissions and features in manifest file

3. Session setup

4. Configure document types

5. Session token flags

6. Language strings

7. Colors

8. Fonts

9. Liveness icons

10. Loading icon

11. Flow skip parameters

12. Environment setup

13. Start Identomat sdk

14. Setup a broadcast receiver to accept data from sdk



### 1. Add identomat library to the project
To integrate identomat library into the project client must place the identomatsdk_{{version}}.aar 
file in app/libs folder.
Library must also be added to the dependencies of module's build.gradle file
```
dependencies {
    ...
    implementation files('libs/identomatsdk_{{version}}.aar')
}
```
Client must also indicate a flatDir property in project's build.gradle file in 
allprojects/repositories section
```
allprojects {
  repositories {
            google()
            jcenter()
            flatDir {
               dirs 'libs/aars'
            }
  }
}
```

### 2. Add permissions and features in manifest file
Identomat sdk needs permissions and camera 2 api feature so they must be declared in application's 
manifest file

```
<uses-feature android:name="android.hardware.camera2.full" android:required="false"/>
<uses-permission android:name="android.permission.READ_INTERNAL_STORAGE" />
<uses-permission android:name="android.permission.WRITE_INTERNAL_STORAGE" />
<uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
<uses-permission android:name="android.permission.CAMERA" />
<uses-permission android:name="android.permission.INTERNET" />
```

### 3. Session setup
To start interacting with identomat sdk client must create and setup the session first.
Client creates a session instance by calling get instance method of SesisonSetup object
```
SessionSetup ses = SessionSetup.getInstance(getResources().getConfiguration().locale.getLanguage());
```
In the session setup client can indicate following properties:
    - Document types
    - Session token flags
    - Language strings
    - Colors
    - Fonts
    - Liveness icons
    - Loading icon
    - Flow skip parameters
    
### 4. Configure document types
Client can configure document types by calling addDocumentType method of SessionSetup object and
passing IdentomatClientDocType enum as a parameter.
Currently IdentomatClientDocType is an enum with 4 values:
```
ID_CARD,PASSPORT,DRIVING_LICENSE,RESIDENCE_PERMIT
```
Example of adding document types
```
ses.addDocumentType(IdentomatClientDocType.ID_CARD);
ses.addDocumentType(IdentomatClientDocType.PASSPORT);
ses.addDocumentType(IdentomatClientDocType.DRIVING_LICENSE);
ses.addDocumentType(IdentomatClientDocType.RESIDENCE_PERMIT);
```

### 5. Session token flags
Session token flags can be setup using SessionSetup methods.
Currently 5 session token flags are available:
```
"language","liveness","skip_face","skip_document","allow_document_upload"
```

Ex:
```
ses.setLanugage({{string}}); // en, es, ka, ru
ses.setLivenss({{boolean}}); // true, false
ses.setSkipFace({{boolean}}); // true, false
ses.setSkipDocument({{boolean}}); // true, false
ses.setAllowDocumentUpload({{boolean}}); // true, false
```

### 6. Language strings
There are two ways of setting ui texts for identomat sdk.
First way is to create strings for the following keys in string.xml file. Keep in mind that client
needs to create a separate string.xml file for each locale that is supperted (en, ka, es, ru).
```
<!--Consent Screen-->
<string name="identomat_agree_page_title">Consent for personal data processing</string>
<string name="identomat_agree">Agree</string>
<string name="identomat_disagree">Disagree</string>

<!--Document Selection-->
<string name="identomat_select_document">Select document</string>
<string name="identomat_id_card">ID card</string>
<string name="identomat_passport">Passport</string>
<string name="identomat_driver_license">Driver License</string>
<string name="identomat_residence_permit">Residence Permit</string>

<!--Photo Upload Type-->
<string name="identomat_capture_method_title">Choose a method</string>
<string name="identomat_take_photo">Take a photo</string>
<string name="identomat_upload_file">Upload a file</string>

<!--No Camera-->
<string name="identomat_camera_access_denied">Camera access denied</string>
<string name="identomat_retry">Retry</string>
<string name="identomat_use_another_device">Use another device</string>

<!--File Upload Section-->
<string name="identomat_card_front_upload">Upload FRONT SIDE of ID CARD</string>
<string name="identomat_card_rear_upload">Upload BACK SIDE of ID CARD</string>
<string name="identomat_driver_license_front_upload">Upload FRONT SIDE of DRIVER LICENSE</string>
<string name="identomat_driver_license_rear_upload">Upload BACK SIDE of DRIVER LICENSE</string>
<string name="identomat_residence_permit_front_upload">Upload FRONT SIDE of RESIDENCE PERMIT</string>
<string name="identomat_residence_permit_rear_upload">Upload BACK SIDE of RESIDENCE PERMIT</string>
<string name="identomat_passport_upload">Upload passport photo page</string>
<string name="identomat_upload_instructions_1">Upload a color image of the entire document</string>
<string name="identomat_upload_instructions_2">JPG or PNG format only</string>
<string name="identomat_upload_instructions_3">Screenshots are not allowed</string>
<string name="identomat_choose_file">Choose a file</string>
<string name="identomat_uploading">Uploading...</string>

<!--Instructions -->
<string name="identomat_face_instructions">Place your FACE within OVAL</string>
<string name="identomat_record_instructions">Place your FACE within OVAL and follow the on-screen instructions</string>
<string name="identomat_card_front_instructions">Scan FRONT side of ID card</string>
<string name="identomat_card_rear_instructions">Scan BACK side of ID card</string>
<string name="identomat_passport_instructions">Passport photo page</string>
<string name="identomat_driver_license_front_instructions">Scan FRONT side of DRIVING LICENSE</string>
<string name="identomat_driver_license_rear_instructions">Scan BACK side of DRIVING LICENSE</string>
<string name="identomat_residence_permit_front_instructions">Scan FRONT side of RESIDENCE PERMIT</string>
<string name="identomat_residence_permit_rear_instructions">Scan BACK side of RESIDENCE PERMIT</string>

<!--Errors and responses-->
<string name="identomat_error">Error</string>
<string name="identomat_document_align">Frame your document</string>
<string name="identomat_document_blurry">Hold still</string>
<string name="identomat_document_type_unknown">Document type unknown</string>
<string name="identomat_document_covered">Document is covered</string>
<string name="identomat_document_face_blurry">Hold still</string>
<string name="identomat_document_face_require_brighter">Low light</string>
<string name="identomat_document_face_too_bright">Avoid direct light</string>
<string name="identomat_document_move_away">Please move document away</string>
<string name="identomat_document_move_closer">Please move document closer</string>
<string name="identomat_document_move_down">Please move document down</string>
<string name="identomat_document_move_left">Please move document to the left</string>
<string name="identomat_document_move_right">Please move document to the right</string>
<string name="identomat_document_move_up">Please move document up</string>
<string name="identomat_document_not_readable">Document not readable</string>
<string name="identomat_document_type_mismatch">Wrong document</string>
<string name="identomat_face_align">Frame your face</string>
<string name="identomat_face_away_from_center">Center your Face</string>
<string name="identomat_face_blurry">Hold still</string>
<string name="identomat_face_far_away">Move closer</string>
<string name="identomat_face_require_brighter">Low light</string>
<string name="identomat_face_too_bright">Avoid direct light</string>
<string name="identomat_face_too_close">Move away</string>
<string name="identomat_no_document_in_image">Frame your document</string>
<string name="identomat_smile_detected">Get neutral face</string>
<string name="identomat_image_not_readable">Image not readable</string>
<string name="identomat_true">Hold still</string>
<string name="identomat_resend">Hold still</string>
<string name="identomat_document_processing">Processing, please wait</string>
<string name="identomat_captured">Captured</string>
<string name="identomat_neutral_expression">Neutral face</string>
<string name="identomat_smile">Smile</string>
<string name="identomat_record_fail_title">Let\'s try again</string>
<string name="identomat_face_looks_fine">Face looks fine</string>
<string name="identomat_retake_photo">Retake photo</string>
<string name="identomat_lets_try">Let\'s try</string>

<!--Video Selfie-->
<string name="identomat_record_begin_title">Get ready for your video selfie</string>
<string name="identomat_record_begin_section_1">Take a neutral expression</string>
<string name="identomat_record_begin_section_2">Smile on this sign</string>
<string name="identomat_record_begin_section_3">Take a neutral expression again</string>
<string name="identomat_verifying">Verifying...</string>
<string name="identomat_im_ready">I\'m ready</string>

<!--Confirm screen-->
<string name="identomat_confirm_photo">Confirm photo is clear and legible</string>
<string name="identomat_card_looks_fine">Card looks fine</string>
<string name="identomat_license_looks_fine">License looks fine</string>
<string name="identomat_permit_looks_fine">Permit looks fine</string>
<string name="identomat_passport_looks_fine">Passport looks fine</string>
<string name="identomat_scan_again">Retake photo</string>
<string name="identomat_upload_another_file">Upload another file</string>

<!--Result-->
<string name="identomat_record_fail_description">But first, please take a look at the instructions</string>
```

Another method to setup texts is passing HashMap to a SessionSetup's setString() method.
Keys for this map should be locales like en,ka,es,ru.
Value of each key is another hashmap that contains string ids and corresponding values.

Example:
```
HashMap<String, HashMap<String, String>> strings = new HashMap();
//english
HashMap<String, String> en = new HashMap();  
en.put("identomat_agree_page_title", "consent text");
en.put("identomat_agree", "agree text");
en.put("identomat_disagree", "disagree text");
strings.put("en", en);

//georgian
HashMap<String, String> ka = new HashMap();  
ka.put("identomat_agree_page_title", "თანხმობის სათაური");
ka.put("identomat_agree", ""თანხმობის ტექსტი");
ka.put("identomat_disagree", "უარყოფის ტექსტი");
strings.put("ka", ka);
    
//spanish
HashMap<String, String> es = new HashMap();
es.put("identomat_agree_page_title", "Texto de consentimiento");
es.put("identomat_agree", "Texto de acuerdo");
es.put("identomat_disagree", "Texto en desacuerdo");
strings.put("es", es);

//russian
HashMap<String, String> ru = new HashMap();
ru.put("identomat_agree_page_title", "Текст согласия");
ru.put("identomat_agree", "Согласительный текст");
ru.put("identomat_disagree", "Несогласный текст");
strings.put("ru", ru);

ses.setStrings(strings);
```

You can notice that keys for each language are the same keys used in string.xml file.

### 7. Colors
Identomat sdk has 12 color settings throughout it ui. It also supports dark mode settings so
dark mode colors should also be specified
```
HashMap<String, String> colors = new HashMap<String, String>();
colors.put("primary_button","#832B8F");
colors.put("background_low", "#F2F2F2");
colors.put("background_high","#FFFFFF");
colors.put("primary_button_text", "#FFFFFF");
colors.put("secondary_button", "#FFFFFF");
colors.put("secondary_button_text", "#832B8F");
colors.put("text_color_header", "#A4A4A4");
colors.put("text_color", "#676767");
colors.put("selector_header", "#832B8F");
colors.put("document_outer","#832B8F");
colors.put("iteration_text","#000000");
colors.put("iteration_outer","#BABABA");
colors.put("background_low_dark", "#011627");
colors.put("background_high_dark", "#102C43");
colors.put("primary_button_dark","#17BEBB");
colors.put("primary_button_text_dark", "#FFFFFF");
colors.put("secondary_button_dark", "#011627");
colors.put("secondary_button_text_dark", "#FFFFFF");
colors.put("text_color_header_dark", "#FFFFFF");
colors.put("text_color_dark", "#FFFFFF");
colors.put("selector_header_dark", "#FFFFFF");
colors.put("document_outer_dark","#17BEBB");
colors.put("iteration_text_dark","#000000");
colors.put("iteration_outer_dark","#BABABA");
ses.setColors(colors);
```
### 8. Fonts
To setup custom fonts use following methods that accept names of font files in 
src/main/assets folder:

```
ses.setHead1Font("fonts/bpg_nino_mtavruli_normal.ttf"); //Large text font
ses.setHead2Font("fonts/helvetica_neue_light.ttf"); //medium text and button font
ses.setBodyFont("fonts/mtavruli.ttf"); //aggreement text and verification message text font
```

### 9. Liveness icons
To setup liveness icons client must use setNeutralIcon and setSmileIcon methods of SessionSetup
object. These methods accept strings as parameter that represent icon resource's name in 
drawables
```
ses.setNeutralIcon("custom_neutral");
ses.setSmileIcon("custom_smile");
```
### 10. Loading icon
To setup verifying and uploading logo client has to pass byte array to SessionSetup's setLogo() method.
   
Example:
```
InputStream inputStream;
try {
    inputStream = getActivity().getAssets().open("img/mask.png");
    byte[] buffer = new byte[8192];
    int bytesRead;
    ByteArrayOutputStream output = new ByteArrayOutputStream();
    while ((bytesRead = inputStream.read(buffer)) != -1) {
          output.write(buffer, 0, bytesRead);
    }
    byte file[] = output.toByteArray();
    ses.setLogo(file);
} catch (IOException e) {
    e.printStackTrace();
}
```

    

    
### 11. Flow skip parameters
Identomat sdk allows skipping different parts of application screens like skipping legal
aggreement screen or liveness instruction screen.
Please use following methods to skip screens mentioned above:

```
ses.skipLegalAgreement();
ses.skipLivenessInstructions();
```

### 12. Setup environment
Client can indicate different servers to work with depending on the environment it needs.
Currently we have three different environments
- DEV
- PROD
- TEST

To setup current environment call setClientEnvironment of SessionSetup instance 
and pass ClientEnvironment enum.

Ex:
```
ses.setClientEnvironment(ClientEnvironment.DEV | ClientEnvironment.PROD | ClientEnvironment.TEST);
```
If this method is not called at all default PROD environment will be used

### 13. Start Identomat sdk
To start identomat sdk call

```
Identomat.getInstance().start(getActivity(), ses, mSessionKey); 
```

where first parameter is a Context, second parameter is a SessionSetup object configured before,
and third parameter is a session token acquired from any other service using company_key and 
flags:
```
"language","liveness","skip_face","skip_document","allow_document_upload"
```

An example of getting a session token could be:
```
public void getSessionToken(){
   try {
       String encodedFlags = URLEncoder.encode(SessionSetup.getInstance(getResources().getConfiguration().locale.getLanguage()).getFlags().toString(), StandardCharsets.UTF_8.toString());
       new NetworkTask("external-api/begin","company_key="+ MainActivity.API_KEY+"&flags="+encodedFlags, (ServerResponseCallback) this, NetworkTask.REQUEST_SESSION_KEY).execute();
   } catch (UnsupportedEncodingException e) {
       e.printStackTrace();
   }
}
```

### 14. Setup a broadcast receiver to accept data from sdk
Identomat SDK broadcasts some data for broadcast receivers in several situations. To receive data from broadcast client
needs to register a broadcastreceiver on "com.identomat.Broadcast" action and retreive extras on receive.
Main extra name here is "idento session key" that is always sent when exiting identomat sdk flow.
In some cases client also receives "idento_result", which represents result of some operation.
Ex:
- 4.1 - Create IdentomatBroadcastReceiver class and implement onReceive method to handle the result
```
public class IdentomatBroadcastReceiver extends BroadcastReceiver {
    public IdentomatBroadcastReceiver() {
    }
    @Override
    public void onReceive(Context context, Intent intent) {
            String result = intent.getStringExtra("idento_result");
            String sessionKey = intent.getStringExtra("idento session key");
            Intent i = new Intent(context,SomeResultActivity.class);
            i.putExtra("RESULT", result);
            i.putExtra("SESSION_KEY", sessionKey);
            context.startActivity(i);
    }
} 
```

- 4.2 - Register broadcast in onCreate() method of a clients activity that calls identomat sdk
``` 
receiver = new IdentomatBroadcastReceiver();
IntentFilter filter = new IntentFilter("com.identomat.Broadcast");
registerReceiver(receiver, filter);
```

- 4.3 - Unregister broadcast in onDestroy() method of a clients activity that calls identomatSDK
``` 
@Override
public void onDestroy(){
    super.onDestroy();
    unregisterReceiver(receiver);
}
```